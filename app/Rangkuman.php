<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rangkuman extends Model
{
    protected $table ='rangkuman';
    protected $fillable = ['kode','nama','kategori','guru_komp'];

    public function siswa()
    {
        return $this->belongsToMany(Siswa::class)->withPivot(['nilai'])->withPivot(['nilai2'])->withPivot(['nilai3'])->withPivot(['nilai4'])->withPivot(['nilai5'])->withPivot(['nilai6'])->withPivot(['nilai7'])->withPivot(['nilai8'])->withPivot(['nilai9'])->withPivot(['nilai10'])->withPivot(['nilai11'])->withPivot(['nilai12'])->withPivot(['nilai13'])->withPivot(['nilai14'])->withPivot(['nilai15'])->withPivot(['nilai16'])->withPivot(['nilai17'])->withTimestamps();
    }

    public function akademik()
    {
        return $this->belongsToMany(Akademik::class)->withTimestamps();
    }
}
