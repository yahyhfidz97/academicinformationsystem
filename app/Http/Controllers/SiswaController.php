<?php

namespace App\Http\Controllers;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use DB;
class SiswaController extends Controller
{
    public function index(Request $request)
    {
       
            //$data_siswa = \App\Siswa::all();
            $data_siswa = DB::table('akademik_siswa')
            ->join('akademik', 'akademik_siswa.akademik_id', '=', 'akademik.id')
            ->join('siswa', 'siswa.id', '=', 'akademik_siswa.siswa_id')
            ->select('siswa.id','siswa.no_idk','siswa.nama_lengkap','siswa.jenis_kelamin','siswa.tempat_lahir','siswa.tanggal_lahir')
            ->where('status',1)
            ->get();
       
            $data_akademik = \App\Akademik::select('tahun_akademik')->where('status', 1)->get();

        
        return view('siswa.index',['data_siswa' => $data_siswa,'data_akademik' => $data_akademik]);
    }

    public function index2(Request $request)
    {
        $data_siswa = \App\Siswa::all();
        $data_akademik = \App\Akademik::select('tahun_akademik')->where('status', 1)->get();

    
    return view('siswa.index',['data_siswa' => $data_siswa,'data_akademik' => $data_akademik]);
    }
    
    public function create(Request $request)
    {
        $this->validate($request,[ 'no_idk' => 'unique:siswa',
            'nama_lengkap' => 'min:5',
            'email' => 'required|email|unique:users',
            'jenis_kelamin' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'avatar' => 'mimes:jpeg,png',
            ]);


        //insert ke tabel users
        $user = new \App\User;
        $user->role = 'siswa';
        $user->name = $request->nama_lengkap;
        $user->email = $request->email;
        $user->password = bcrypt('rahasia');
        $user->remember_token = Str::random(60);
        $user->save();

        //insert ke tabel siswa
        $request->request->add(['user_id' => $user->id]);
        $siswa = \App\Siswa::create($request->all());
        if($request->hasFile('avatar')){
            $request->file('avatar')->move('images/',$request->file('avatar')->getClientOriginalName());
            $siswa->avatar = $request->file('avatar')->getClientOriginalName();
            $siswa->save();
        }
        return redirect('/siswa')->with('sukses','Data berhasil ditambahkan');
    }

    public function edit($id)
    {
        $siswa = \App\Siswa::find($id);
        $data_akademik = \App\Akademik::select('tahun_akademik')->where('status', 1)->get();
        $akademikisi = \App\Akademik::select('id','tahun_akademik','status')->where('status', 1)->get();
        
        return view('siswa/edit',['siswa' => $siswa,'data_akademik' => $data_akademik, 'akademikisi' => $akademikisi]);
    }

    public function update(Request $request,$id)
    {
        //dd($request->all());
        $siswa = \App\Siswa::find($id);
        $siswa->update($request->all());
        if($siswa->akademik()->where('akademik_id' ,$request->akademik)->exists()){
            return redirect('/siswa')->with('error', 'Data siswa sudah ada');
        }
        $siswa ->akademik()->attach($request->akademik);
        if($request->hasFile('avatar')){
            $request->file('avatar')->move('images/',$request->file('avatar')->getClientOriginalName());
            $siswa->avatar = $request->file('avatar')->getClientOriginalName();
            $siswa->save();
        }
        return redirect('/siswa')->with('sukses', 'Data berhasil diupdate');
    }

    public function delete($id)
    {
        $siswa = \App\Siswa::find($id);
        $siswa->delete($siswa);
        return redirect('/siswa')->with('sukses','Data berhasil dihapus');
    }

    public function profile($id)
    {
        $siswa = \App\Siswa::find($id);
        $matapelajaran = \App\Rangkuman::all();
        $indikatorisi = \App\Indikator::all();
        $data_akademik = \App\Akademik::select('tahun_akademik')->where('status', 1)->get();
        //dd($mapel);
        //dd($indikator);
        return view('siswa.profile',['siswa' => $siswa, 'matapelajaran' => $matapelajaran, 'indikatorisi' => $indikatorisi,'data_akademik' => $data_akademik]);
    }

    public function profile2($id)
    {
        $siswa = \App\Siswa::find($id);
        $matapelajaran = \App\Rangkuman::all();
        $indikatorisi = \App\Indikator::all();
        $data_akademik = \App\Akademik::select('tahun_akademik')->where('status', 1)->get();
        //dd($mapel);
        //dd($indikator);
        return view('siswa.profile2',['siswa' => $siswa, 'matapelajaran' => $matapelajaran, 'indikatorisi' => $indikatorisi,'data_akademik' => $data_akademik]);
    }

    public function addnilai(Request $request,$idsiswa)
    {
        $siswa = \App\Siswa::find($idsiswa);
        if($siswa->rangkuman()->where('rangkuman_id' ,$request->rangkuman)->exists()){
            return redirect('siswa/' .$idsiswa. '/profile')->with('error', 'Data rangkuman sudah ada');
        }
        $siswa->rangkuman()->attach($request->rangkuman, ['nilai' => $request->nilai]);

        return redirect('siswa/' .$idsiswa. '/profile')->with('sukses', 'Data nilai berhasil ditambahkan');
    }


    public function deletenilai($idsiswa,$idmapel){
        $siswa = \App\Siswa::find($idsiswa);
        $siswa->rangkuman()->detach($idmapel);
        return redirect()->back()->with('sukses','Data berhasil dihapus');
    }


    public function addindikator(Request $request,$idsiswa){
        $siswa = \App\Siswa::find($idsiswa);
        if($siswa->indikator()->where('indikator_id' ,$request->indikator)->exists()){
            return redirect('siswa/' .$idsiswa. '/profile')->with('error', 'Data indikator sudah ada');
        }
        $siswa->indikator()->attach($request->indikator, ['isi_indikator' => $request->isi_indikator]);

        return redirect('siswa/' .$idsiswa. '/profile')->with('sukses', 'Data nilai berhasil ditambahkan');
    }

    public function deleteindikator($idsiswa,$idindikator){
        $siswa = \App\Siswa::find($idsiswa);
        $siswa->indikator()->detach($idindikator);
        return redirect()->back()->with('sukses','Data berhasil dihapus');
    }
    
    public function kelas(Request $request)
    {
        $data_siswa = \App\Siswa::select('id','no_idk','nama_lengkap','kelas')->where('kelas', 'TK A')->get();
        $data_guru = \App\Guru::select('no_id_pegawai','nama_lengkap','alamat')->where('wali', 'Wali Kelas A')->get();
        $data_akademik = \App\Akademik::select('tahun_akademik')->where('status', 1)->get();
        $data_mapel = \App\Rangkuman::all();
    
    return view('siswa.kelas',['data_siswa' => $data_siswa,'data_akademik' => $data_akademik,'data_guru' => $data_guru,'data_mapel' => $data_mapel]);
    }

    public function kelas2(Request $request)
    {
        $data_siswa = \App\Siswa::select('id','no_idk','nama_lengkap','kelas')->where('kelas', 'TK B')->get();
        $data_guru = \App\Guru::select('no_id_pegawai','nama_lengkap','alamat')->where('wali', 'Wali Kelas B')->get();
        $data_akademik = \App\Akademik::select('tahun_akademik')->where('status', 1)->get();
        $data_mapel = \App\Rangkuman::all();

    
    return view('siswa.kelas2',['data_siswa' => $data_siswa,'data_akademik' => $data_akademik,'data_guru' => $data_guru,'data_mapel' => $data_mapel]);
    }
}
