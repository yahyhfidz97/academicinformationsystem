<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AkademikController extends Controller
{
    public function index(Request $request)
    {
        
        $data_akademik = \App\Akademik::select('tahun_akademik')->where('status', 1)->get();
        $data_akademik2 = \App\Akademik::get();

            //return view('akademik.index',['data_akademik' => $data_akademik]);
            return view('akademik.index',compact('data_akademik','data_akademik2'));
    }

    public function changeStatus(Request $request)
    {
        $user = \App\Akademik::find($request->user_id);
        $user ->where('status', 1) ->update(['status' => 0]);

        $user->status = $request->status;
        $user->save();

        return response()->json(['success'=>'Status change successfully.']);
    }

    public function create(Request $request)
    {
        $indikator = \App\Akademik::create($request->all());
        $indikator->save();
        return redirect('/akademik')->with('sukses','Data berhasil ditambahkan');
    }

    public function edit($id)
    {
        $data_akademik = \App\Akademik::select('tahun_akademik')->where('status', 1)->get();
        $editakademik = \App\Akademik::find($id);
        return view('akademik/edit',['editakademik' => $editakademik, 'data_akademik' => $data_akademik]);
    }

    public function update(Request $request,$id)
    {
    
        $editakademik = \App\Akademik::find($id);
        $editakademik->update($request->all());
        $editakademik->save();
        
        return redirect('/akademik')->with('sukses', 'Data berhasil diupdate');
    }

    public function delete($id)
    {
        $delakademik = \App\Akademik::find($id);
        $delakademik->delete($delakademik);
        return redirect('/akademik')->with('sukses','Data berhasil dihapus');
    }

}
