<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::group(['middleware'=>['auth','checkRole:admin,guru,guru2']],function(){

Route::post('/siswa/{id}/editnilai','ApiController@editnilai');
Route::post('/siswa/{id}/editnilaiw2','ApiController@editnilaiw2');
Route::post('/siswa/{id}/editnilaiw3','ApiController@editnilaiw3');
Route::post('/siswa/{id}/editnilaiw4','ApiController@editnilaiw4');
Route::post('/siswa/{id}/editnilaiw5','ApiController@editnilaiw5');
Route::post('/siswa/{id}/editnilaiw6','ApiController@editnilaiw6');
Route::post('/siswa/{id}/editnilaiw7','ApiController@editnilaiw7');
Route::post('/siswa/{id}/editnilaiw8','ApiController@editnilaiw8');
Route::post('/siswa/{id}/editnilaiw9','ApiController@editnilaiw9');
Route::post('/siswa/{id}/editnilaiw10','ApiController@editnilaiw10');
Route::post('/siswa/{id}/editnilaiw11','ApiController@editnilaiw11');
Route::post('/siswa/{id}/editnilaiw12','ApiController@editnilaiw12');
Route::post('/siswa/{id}/editnilaiw13','ApiController@editnilaiw13');
Route::post('/siswa/{id}/editnilaiw14','ApiController@editnilaiw14');
Route::post('/siswa/{id}/editnilaiw15','ApiController@editnilaiw15');
Route::post('/siswa/{id}/editnilaiw16','ApiController@editnilaiw16');
Route::post('/siswa/{id}/editnilaiw17','ApiController@editnilaiw17');

Route::post('/rangkuman/{id}/editpen1','ApiController@editpen1');
Route::post('/rangkuman/{id}/editpen2','ApiController@editpen2');
Route::post('/rangkuman/{id}/editpen3','ApiController@editpen3');
Route::post('/rangkuman/{id}/editpen4','ApiController@editpen4');
Route::post('/rangkuman/{id}/editpen5','ApiController@editpen5');
Route::post('/rangkuman/{id}/editpen6','ApiController@editpen6');
Route::post('/rangkuman/{id}/editpen7','ApiController@editpen7');
Route::post('/rangkuman/{id}/editpen8','ApiController@editpen8');
Route::post('/rangkuman/{id}/editpen9','ApiController@editpen9');
Route::post('/rangkuman/{id}/editpen10','ApiController@editpen10');
Route::post('/rangkuman/{id}/editpen11','ApiController@editpen11');
Route::post('/rangkuman/{id}/editpen12','ApiController@editpen12');
Route::post('/rangkuman/{id}/editpen13','ApiController@editpen13');
Route::post('/rangkuman/{id}/editpen14','ApiController@editpen14');
Route::post('/rangkuman/{id}/editpen15','ApiController@editpen15');
Route::post('/rangkuman/{id}/editpen16','ApiController@editpen16');
Route::post('/rangkuman/{id}/editpen17','ApiController@editpen17');



Route::post('/siswa/{id}/editindikator','ApiController@editindikator');
// });