<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function editnilai(Request $request, $id)
    {
        $siswa = \App\Siswa::find($id);
        $siswa->rangkuman()->updateExistingPivot($request->pk,['nilai'=> $request->value]);
    }

    public function editnilaiw2(Request $request, $id)
    {
        $siswa = \App\Siswa::find($id);
        $siswa->rangkuman()->updateExistingPivot($request->pk,['nilai2'=> $request->value]);
    }

    public function editnilaiw3(Request $request, $id)
    {
        $siswa = \App\Siswa::find($id);
        $siswa->rangkuman()->updateExistingPivot($request->pk,['nilai3'=> $request->value]);
    }


    public function editnilaiw4(Request $request, $id)
    {
        $siswa = \App\Siswa::find($id);
        $siswa->rangkuman()->updateExistingPivot($request->pk,['nilai4'=> $request->value]);
    }

    public function editnilaiw5(Request $request, $id)
    {
        $siswa = \App\Siswa::find($id);
        $siswa->rangkuman()->updateExistingPivot($request->pk,['nilai5'=> $request->value]);
    }

    public function editnilaiw6(Request $request, $id)
    {
        $siswa = \App\Siswa::find($id);
        $siswa->rangkuman()->updateExistingPivot($request->pk,['nilai6'=> $request->value]);
    }

    public function editnilaiw7(Request $request, $id)
    {
        $siswa = \App\Siswa::find($id);
        $siswa->rangkuman()->updateExistingPivot($request->pk,['nilai7'=> $request->value]);
    }

    public function editnilaiw8(Request $request, $id)
    {
        $siswa = \App\Siswa::find($id);
        $siswa->rangkuman()->updateExistingPivot($request->pk,['nilai8'=> $request->value]);
    }

    public function editnilaiw9(Request $request, $id)
    {
        $siswa = \App\Siswa::find($id);
        $siswa->rangkuman()->updateExistingPivot($request->pk,['nilai9'=> $request->value]);
    }

    public function editnilaiw10(Request $request, $id)
    {
        $siswa = \App\Siswa::find($id);
        $siswa->rangkuman()->updateExistingPivot($request->pk,['nilai10'=> $request->value]);
    }

    public function editnilaiw11(Request $request, $id)
    {
        $siswa = \App\Siswa::find($id);
        $siswa->rangkuman()->updateExistingPivot($request->pk,['nilai11'=> $request->value]);
    }

    public function editnilaiw12(Request $request, $id)
    {
        $siswa = \App\Siswa::find($id);
        $siswa->rangkuman()->updateExistingPivot($request->pk,['nilai12'=> $request->value]);
    }

    public function editnilaiw13(Request $request, $id)
    {
        $siswa = \App\Siswa::find($id);
        $siswa->rangkuman()->updateExistingPivot($request->pk,['nilai13'=> $request->value]);
    }

    public function editnilaiw14(Request $request, $id)
    {
        $siswa = \App\Siswa::find($id);
        $siswa->rangkuman()->updateExistingPivot($request->pk,['nilai14'=> $request->value]);
    }

    public function editnilaiw15(Request $request, $id)
    {
        $siswa = \App\Siswa::find($id);
        $siswa->rangkuman()->updateExistingPivot($request->pk,['nilai15'=> $request->value]);
    }

    public function editnilaiw16(Request $request, $id)
    {
        $siswa = \App\Siswa::find($id);
        $siswa->rangkuman()->updateExistingPivot($request->pk,['nilai16'=> $request->value]);
    }

    public function editnilaiw17(Request $request, $id)
    {
        $siswa = \App\Siswa::find($id);
        $siswa->rangkuman()->updateExistingPivot($request->pk,['nilai17'=> $request->value]);
    }

    

    
    public function editpen1(Request $request, $id)
    {
        $rangkuman = \App\Rangkuman::find($id);
        $rangkuman->siswa()->updateExistingPivot($request->pk,['nilai'=> $request->value]);
    }

    public function editpen2(Request $request, $id)
    {
        $rangkuman = \App\Rangkuman::find($id);
        $rangkuman->siswa()->updateExistingPivot($request->pk,['nilai2'=> $request->value]);
    }

    public function editpen3(Request $request, $id)
    {
        $rangkuman = \App\Rangkuman::find($id);
        $rangkuman->siswa()->updateExistingPivot($request->pk,['nilai3'=> $request->value]);
    }

    public function editpen4(Request $request, $id)
    {
        $rangkuman = \App\Rangkuman::find($id);
        $rangkuman->siswa()->updateExistingPivot($request->pk,['nilai4'=> $request->value]);
    }

    public function editpen5(Request $request, $id)
    {
        $rangkuman = \App\Rangkuman::find($id);
        $rangkuman->siswa()->updateExistingPivot($request->pk,['nilai5'=> $request->value]);
    }

    public function editpen6(Request $request, $id)
    {
        $rangkuman = \App\Rangkuman::find($id);
        $rangkuman->siswa()->updateExistingPivot($request->pk,['nilai6'=> $request->value]);
    }

    public function editpen7(Request $request, $id)
    {
        $rangkuman = \App\Rangkuman::find($id);
        $rangkuman->siswa()->updateExistingPivot($request->pk,['nilai7'=> $request->value]);
    }

    public function editpen8(Request $request, $id)
    {
        $rangkuman = \App\Rangkuman::find($id);
        $rangkuman->siswa()->updateExistingPivot($request->pk,['nilai8'=> $request->value]);
    }

    public function editpen9(Request $request, $id)
    {
        $rangkuman = \App\Rangkuman::find($id);
        $rangkuman->siswa()->updateExistingPivot($request->pk,['nilai9'=> $request->value]);
    }

    public function editpen10(Request $request, $id)
    {
        $rangkuman = \App\Rangkuman::find($id);
        $rangkuman->siswa()->updateExistingPivot($request->pk,['nilai10'=> $request->value]);
    }

    public function editpen11(Request $request, $id)
    {
        $rangkuman = \App\Rangkuman::find($id);
        $rangkuman->siswa()->updateExistingPivot($request->pk,['nilai11'=> $request->value]);
    }

    public function editpen12(Request $request, $id)
    {
        $rangkuman = \App\Rangkuman::find($id);
        $rangkuman->siswa()->updateExistingPivot($request->pk,['nilai12'=> $request->value]);
    }

    public function editpen13(Request $request, $id)
    {
        $rangkuman = \App\Rangkuman::find($id);
        $rangkuman->siswa()->updateExistingPivot($request->pk,['nilai13'=> $request->value]);
    }

    public function editpen14(Request $request, $id)
    {
        $rangkuman = \App\Rangkuman::find($id);
        $rangkuman->siswa()->updateExistingPivot($request->pk,['nilai14'=> $request->value]);
    }

    public function editpen15(Request $request, $id)
    {
        $rangkuman = \App\Rangkuman::find($id);
        $rangkuman->siswa()->updateExistingPivot($request->pk,['nilai15'=> $request->value]);
    }

    public function editpen16(Request $request, $id)
    {
        $rangkuman = \App\Rangkuman::find($id);
        $rangkuman->siswa()->updateExistingPivot($request->pk,['nilai16'=> $request->value]);
    }

    public function editpen17(Request $request, $id)
    {
        $rangkuman = \App\Rangkuman::find($id);
        $rangkuman->siswa()->updateExistingPivot($request->pk,['nilai17'=> $request->value]);
    }

    public function editindikator(Request $request, $id)
    {
        $siswa = \App\Siswa::find($id);
        $siswa->indikator()->updateExistingPivot($request->pk,['isi_indikator'=> $request->value]);
    }
}
