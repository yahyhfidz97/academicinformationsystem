@extends('layouts.master')
@section('header')
<style>body {padding:30px}
.print-area {border:1px solid white;padding:1em;margin:0 0 1em}</style>
@stop
@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                            <div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Data Indikator <br>@foreach ($data_akademik as $item)
									T.A. {{$item->tahun_akademik}}
									@endforeach</h3>
                                    <br>
                                    <!--<input class="form-control" type="search" placeholder="Search" aria-label="Search">
                                    <button class="btn btn-outline-success" type="submit">Search</button>-->
                                    <div class="right">
                                    <button type="button" class="btn" data-toggle="modal" data-target="#exampleModal"><i class="lnr lnr-plus-circle"></i>
                                    </button>
                                    <button type="button" class="btn no-print" onclick="javascript:printDiv('print-area-2');"><i class="lnr lnr-printer"></i></button>
                                    </div>
								</div>
								<div class="panel-body print-area" id="print-area-2">
									<table class="table table-hover" border="1" >
										<thead>
											<tr>
                                                <th>KODE</th>
                                                <th>NAMA INDIKATOR</th>
                                                
                                                <th>AKSI</th>
											</tr>
										</thead>
										<tbody>
                                        @foreach($data_indikator as $indikator)
                                        <tr>
                                            <td><a href="/indikator/{{$indikator->id}}/profile">{{$indikator->kode}}</a></td>
                                            <td><a href="/indikator/{{$indikator->id}}/profile">{{$indikator->nama_indikator}}</a></td>
                                            
                                            <td><a href="/indikator/{{$indikator->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                                <a href="/indikator/{{$indikator->id}}/delete" class="btn btn-danger btn-sm" onClick="return confirm('Yakin mau dihapus?')">Delete</a></td>
                                        </tr>
                                        @endforeach
										</tbody>
									</table>
								</div>
                                <textarea id="printing-css" style="display:none;">html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:before,blockquote:after,q:before,q:after{content:'';content:none}table{border-collapse:collapse;border-spacing:0}body{font:normal normal .8125em/1.4 Arial,Sans-Serif;background-color:white;color:#333}strong,b{font-weight:bold}cite,em,i{font-style:italic}a{text-decoration:none}a:hover{text-decoration:underline}a img{border:none}abbr,acronym{border-bottom:1px dotted;cursor:help}sup,sub{vertical-align:baseline;position:relative;top:-.4em;font-size:86%}sub{top:.4em}small{font-size:86%}kbd{font-size:80%;border:1px solid #999;padding:2px 5px;border-bottom-width:2px;border-radius:3px}mark{background-color:#ffce00;color:black}p,blockquote,pre,table,figure,hr,form,ol,ul,dl{margin:1.5em 0}hr{height:1px;border:none;background-color:#666}h1,h2,h3,h4,h5,h6{font-weight:bold;line-height:normal;margin:1.5em 0 0}h1{font-size:200%}h2{font-size:180%}h3{font-size:160%}h4{font-size:140%}h5{font-size:120%}h6{font-size:100%}ol,ul,dl{margin-left:3em}ol{list-style:decimal outside}ul{list-style:disc outside}li{margin:.5em 0}dt{font-weight:bold}dd{margin:0 0 .5em 2em}input,button,select,textarea{font:inherit;font-size:100%;line-height:normal;vertical-align:baseline}textarea{display:block;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}pre,code{font-family:"Courier New",Courier,Monospace;color:inherit}pre{white-space:pre;word-wrap:normal;overflow:auto}blockquote{margin-left:2em;margin-right:2em;border-left:4px solid #ccc;padding-left:1em;font-style:italic}table[border="1"] th,table[border="1"] td,table[border="1"] caption{border:1px solid;padding:.5em 1em;text-align:left;vertical-align:top}th{font-weight:bold}table[border="1"] caption{border:none;font-style:italic}.no-print{display:none}</textarea>
                                <iframe id="printing-frame" name="print_frame" src="about:blank" style="display:none;"></iframe>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
<!-- Modal Indikator-->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="/indikator/create" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}

                        <div class="mb-3 {{$errors->has('kode') ? ' has-error' : ''}}">
                            <label for="exampleInputEmail1" class="form-label">Kode</label>
                            <input name="kode" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Kode" 
                            value="{{old('kode')}}">
                            @if($errors->has('kode'))
                            <span class="help-block">{{$errors->first('kode')}}</span>
                            @endif
                        </div>

                        <div class="mb-3 {{$errors->has('nama_indikator') ? ' has-error' : ''}}">
                            <label for="exampleInputEmail1" class="form-label">Nama Indikator</label>
                            <input name="nama_indikator" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Indikator" value="{{old('nama_indikator')}}">
                            @if($errors->has('nama_indikator'))
                            <span class="help-block">{{$errors->first('nama_indikator')}}</span>
                            @endif
                        </div>


                        

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>

        @endsection    

        @section('footer')
        <script>
         function printDiv(elementId) {
        var a = document.getElementById('printing-css').value;
        var b = document.getElementById(elementId).innerHTML;
        window.frames["print_frame"].document.title = document.title;
        window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + b;
        window.frames["print_frame"].window.focus();
        window.frames["print_frame"].window.print();
        }
        
        </script>
        @stop
