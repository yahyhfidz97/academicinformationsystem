<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'siswa';
    protected $fillable = ['no_idk','nama_lengkap','jenis_kelamin','tempat_lahir','tanggal_lahir','alamat','kelas','tahun_akademik','avatar','user_id'];
    

    public function getAvatar()
    {
        if(!$this->avatar){
            return asset('images/default.jpg');
        }
        return asset('images/' .$this->avatar);
    }

    public function rangkuman(){
        return $this->belongsToMany(Rangkuman::class)->withPivot(['nilai'])->withPivot(['nilai2'])->withPivot(['nilai3'])->withPivot(['nilai4'])->withPivot(['nilai5'])->withPivot(['nilai6'])->withPivot(['nilai7'])->withPivot(['nilai8'])->withPivot(['nilai9'])->withPivot(['nilai10'])->withPivot(['nilai11'])->withPivot(['nilai12'])->withPivot(['nilai13'])->withPivot(['nilai14'])->withPivot(['nilai15'])->withPivot(['nilai16'])->withPivot(['nilai17'])->withTimestamps();
    }

    public function indikator(){
        return $this->belongsToMany(Indikator::class)->withPivot(['isi_indikator'])->withTimestamps();
    }

    public function akademik()
    {
        return $this->belongsToMany(Akademik::class)->withTimestamps();
    }
   
}
