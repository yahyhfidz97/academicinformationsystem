@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Inputs</h3>
								</div>
								<div class="panel-body">
                                <form action="/guru/{{$guru->id}}/update" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}

                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Nomor Pegawai</label>
                            <input name="no_id_pegawai" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nomor Pegawai" value="{{$guru->no_id_pegawai}}">
                        </div>

                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Nama Lengkap</label>
                            <input name="nama_lengkap" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Lengkap" value="{{$guru->nama_lengkap}}">
                        </div>

                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Tempat lahir</label>
                            <input name="tempat_lahir" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tempat Lahir" value="{{$guru->tempat_lahir}}">
                        </div>

                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Tanggal lahir</label>
                            <input name="tanggal_lahir" type="date" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tanggal Lahir" value="{{$guru->tanggal_lahir}}">
                        </div>

                        <label for="jenis_kelamin" class="form-label">Jenis Kelamin</label>
                                <select id="jenis_kelamin" class="form-control" name="jenis_kelamin">
                                    <option selected value="L" @if ($guru -> jenis_kelamin == 'L') selected @endif
                                        >Laki-Laki</option>
                                    <option value="P" @if ($guru -> jenis_kelamin == 'P') selected @endif >Perempuan
                                    </option>
                                </select>

                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Pendidikan</label>
                            <input name="pendidikan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Pendidikan" value="{{$guru->pendidikan}}">
                        </div>

                        <label for="wali" class="form-label">Wali</label>
                                <select id="wali" class="form-control" name="wali">
                                    <option selected value="Wali Kelas A" @if ($guru -> wali == 'Wali Kelas A') selected @endif >Wali Kelas A</option>
                                    <option value="Wali Kelas B" @if ($guru -> wali == 'Wali Kelas B') selected @endif >Wali Kelas B</option>
                                    <option value="Tidak ada" @if ($guru -> wali == 'Tidak ada') selected @endif >Tidak ada</option>
                                </select>

                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Alamat</label>
                            <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3">{{$guru->alamat}}</textarea>
                        </div>
                        
                        <button type="submit" class="btn btn-warning">Update</button>
                    </form>

						</div>
				    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop