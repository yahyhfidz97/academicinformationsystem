@extends('layouts.master')
@section('header')
<style>body {padding:30px}
.print-area {border:1px solid white;padding:1em;margin:0 0 1em}</style>
@stop
@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                            <div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Data Guru <br>@foreach ($data_akademik as $item)
									T.A. {{$item->tahun_akademik}}
									@endforeach</h3>
                                    <br>
                                    <!--<input class="form-control" type="search" placeholder="Search" aria-label="Search">
                                    <button class="btn btn-outline-success" type="submit">Search</button>-->
                                    <div class="right">
                                    <button type="button" class="btn" data-toggle="modal" data-target="#exampleModal"><i class="lnr lnr-plus-circle"></i>
                                    </button>
                                    <button type="button" class="btn no-print" onclick="javascript:printDiv('print-area-2');"><i class="lnr lnr-printer"></i></button>
                                    </div>
								</div>
								<div class="panel-body print-area" id="print-area-2">
									<table class="table table-hover" border="1" >
										<thead>
											<tr>
                                                <th>NOMOR PEGAWAI</th>
                                                <th>NAMA LENGKAP</th>
                                                <th>TEMPAT LAHIR</th>
                                                <th>TANGGAL LAHIR</th>
                                                <th>JENIS KELAMIN</th>
                                                <th>PENDIDIKAN</th>
                                                <th>JABATAN</th>
                                                <th>AKSI</th>
											</tr>
										</thead>
										<tbody>
                                        @foreach($data_guru as $guru)
                                        <tr>
                                            <td>{{$guru->no_id_pegawai}}</td>
                                            <td>{{$guru->nama_lengkap}}</td>
                                            <td>{{$guru->tempat_lahir}}</td>
                                            <td>{{\Carbon\Carbon::parse($guru->tanggal_lahir)->format('d/m/Y')}}</td>
                                            <td>{{$guru->jenis_kelamin}}</td>
                                            <td>{{$guru->pendidikan}}</td>
                                            <td>{{$guru->wali}}</td>
                                            <td><a href="/guru/{{$guru->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                                <a href="/guru/{{$guru->id}}/delete" class="btn btn-danger btn-sm" onClick="return confirm('Yakin mau dihapus?')">Delete</a></td>
                                        </tr>
                                        @endforeach
										</tbody>
									</table>
								</div>
                                <textarea id="printing-css" style="display:none;">html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:before,blockquote:after,q:before,q:after{content:'';content:none}table{border-collapse:collapse;border-spacing:0}body{font:normal normal .8125em/1.4 Arial,Sans-Serif;background-color:white;color:#333}strong,b{font-weight:bold}cite,em,i{font-style:italic}a{text-decoration:none}a:hover{text-decoration:underline}a img{border:none}abbr,acronym{border-bottom:1px dotted;cursor:help}sup,sub{vertical-align:baseline;position:relative;top:-.4em;font-size:86%}sub{top:.4em}small{font-size:86%}kbd{font-size:80%;border:1px solid #999;padding:2px 5px;border-bottom-width:2px;border-radius:3px}mark{background-color:#ffce00;color:black}p,blockquote,pre,table,figure,hr,form,ol,ul,dl{margin:1.5em 0}hr{height:1px;border:none;background-color:#666}h1,h2,h3,h4,h5,h6{font-weight:bold;line-height:normal;margin:1.5em 0 0}h1{font-size:200%}h2{font-size:180%}h3{font-size:160%}h4{font-size:140%}h5{font-size:120%}h6{font-size:100%}ol,ul,dl{margin-left:3em}ol{list-style:decimal outside}ul{list-style:disc outside}li{margin:.5em 0}dt{font-weight:bold}dd{margin:0 0 .5em 2em}input,button,select,textarea{font:inherit;font-size:100%;line-height:normal;vertical-align:baseline}textarea{display:block;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}pre,code{font-family:"Courier New",Courier,Monospace;color:inherit}pre{white-space:pre;word-wrap:normal;overflow:auto}blockquote{margin-left:2em;margin-right:2em;border-left:4px solid #ccc;padding-left:1em;font-style:italic}table[border="1"] th,table[border="1"] td,table[border="1"] caption{border:1px solid;padding:.5em 1em;text-align:left;vertical-align:top}th{font-weight:bold}table[border="1"] caption{border:none;font-style:italic}.no-print{display:none}</textarea>
                                <iframe id="printing-frame" name="print_frame" src="about:blank" style="display:none;"></iframe>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
<!-- Modal guru -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="/guru/create" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}

                        <div class="mb-3 {{$errors->has('no_id_pegawai') ? ' has-error' : ''}}">
                            <label for="exampleInputEmail1" class="form-label">NOMOR PEGAWAI</label>
                            <input name="no_id_pegawai" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nomor Pegawai" 
                            value="{{old('no_id_pegawai')}}">
                            @if($errors->has('no_id_pegawai'))
                            <span class="help-block">{{$errors->first('no_id_pegawai')}}</span>
                            @endif
                        </div>

                        <div class="mb-3 {{$errors->has('nama_lengkap') ? ' has-error' : ''}}">
                            <label for="exampleInputEmail1" class="form-label">Nama Lengkap</label>
                            <input name="nama_lengkap" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Lengkap" value="{{old('nama_lengkap')}}">
                            @if($errors->has('nama_lengkap'))
                            <span class="help-block">{{$errors->first('nama_lengkap')}}</span>
                            @endif
                        </div>

                        <div class="mb-3 {{$errors->has('tempat_lahir') ? ' has-error' : ''}}">
                            <label for="exampleInputEmail1" class="form-label">Tempat Lahir</label>
                            <input name="tempat_lahir" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tempat Lahir" value="{{old('tempat_lahir')}}">
                            @if($errors->has('tempat_lahir'))
                            <span class="help-block">{{$errors->first('tempat_lahir')}}</span>
                            @endif
                        </div>

                        <div class="mb-3 {{$errors->has('tanggal_lahir') ? ' has-error' : ''}}">
                            <label for="exampleInputEmail1" class="form-label">Tanggal Lahir</label>
                            <input name="tanggal_lahir" type="date" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tanggal Lahir" value="{{old('tanggal_lahir')}}">
                            @if($errors->has('tanggal_lahir'))
                            <span class="help-block">{{$errors->first('tanggal_lahir')}}</span>
                            @endif
                        </div>

                        <div class="mb-3 {{$errors->has('jenis_kelamin') ? ' has-error' : ''}}">
                            <label for="exampleInputEmail1" class="form-label">Pilih Jenis Kelamin</label>
                            <br>
                        <select name="jenis_kelamin" class="mb-3" aria-label="Default select example">
                            <option selected>Pilih jenis kelamin</option>
                            <option value="L"{{(old('jenis_kelamin') == 'L') ? ' selected' : ''}}>Laki-laki</option>
                            <option value="P"{{(old('jenis_kelamin') == 'P') ? ' selected' : ''}}>Perempuan</option>
                        </select>
                        @if($errors->has('jenis_kelamin'))
                            <span class="help-block">{{$errors->first('jenis_kelamin')}}</span>
                            @endif
                        </div>

                        <div class="mb-3 {{$errors->has('email') ? ' has-error' : ''}}">
                            <label for="exampleInputEmail1" class="form-label">Email</label>
                            <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email" value="{{old('email')}}">
                            @if($errors->has('email'))
                            <span class="help-block">{{$errors->first('email')}}</span>
                            @endif
                        </div>

                        

                        <div class="mb-3 {{$errors->has('pendidikan') ? ' has-error' : ''}}">
                            <label for="exampleInputEmail1" class="form-label">Pendidikan</label>
                            <input name="pendidikan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Pendidikan" value="{{old('pendidikan')}}">
                            @if($errors->has('pendidikan'))
                            <span class="help-block">{{$errors->first('pendidikan')}}</span>
                            @endif
                        </div>

                        <div class="mb-3 {{$errors->has('wali') ? ' has-error' : ''}}">
                            <label for="exampleInputEmail1" class="form-label">Wali</label>
                            <br>
                        <select name="wali" class="mb-3" aria-label="Default select example">
                            <option selected>Pilih Kelas</option>
                            <option value="Wali Kelas A"{{(old('wali') == '1') ? ' selected' : ''}}>Wali Kelas A</option>
                            <option value="Wali Kelas B"{{(old('wali') == '2') ? ' selected' : ''}}>Wali Kelas B</option>
                            <option value="Tidak ada"{{(old('wali') == '0') ? ' selected' : ''}}>Tidak ada</option>
                        </select>
                        @if($errors->has('wali'))
                            <span class="help-block">{{$errors->first('wali')}}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1" class="form-label">Alamat</label>
                            <textarea name="alamat" class="form-control" id="floatingTextarea">{{old('alamat')}}</textarea>
                        </div>

                        

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>

        @endsection    

        @section('footer')
        <script>
         function printDiv(elementId) {
        var a = document.getElementById('printing-css').value;
        var b = document.getElementById(elementId).innerHTML;
        window.frames["print_frame"].document.title = document.title;
        window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + b;
        window.frames["print_frame"].window.focus();
        window.frames["print_frame"].window.print();
        }
        
        </script>
        @stop
