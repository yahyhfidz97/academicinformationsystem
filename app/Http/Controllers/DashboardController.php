<?php

namespace App\Http\Controllers;

use App\Akademik;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        
            $data_siswa = \App\Siswa::all();
            $data_guru = \App\Guru::all();
            $data_indikator = \App\Indikator::all();
            $data_rangkuman = \App\Rangkuman::all();
            $data_akademik = \App\Akademik::select('tahun_akademik')->where('status', 1)->get();
        
            return view('dashboards.index',['data_akademik' => $data_akademik, 'data_siswa' => $data_siswa,'data_guru' => $data_guru,'data_indikator' => $data_indikator,'data_rangkuman' => $data_rangkuman]);
    }


    public function create(Request $request)
    {
        $data_akademik = \App\Akademik::create($request->all());
        if($request->hasFile('avatar')){
            $request->file('avatar')->move('images/',$request->file('avatar')->getClientOriginalName());
            $data_akademik->avatar = $request->file('avatar')->getClientOriginalName();
            $data_akademik->save();
        }
        return redirect('/dashboards')->with('sukses','Data berhasil ditambahkan');
    }

    public function edit($id)
    {
        $data_akademik = \App\Akademik::all();
        $data_akademik = \App\Akademik::find($id);
        return view('dashboards/edit',['data_akademik' => $data_akademik]);
    }

    public function update(Request $request,$id)
    {
        //dd($request->all());
        $data_akademik = \App\Akademik::find($id);
        $data_akademik->update($request->all());
        // if($request->hasFile('avatar')){
        //     $request->file('avatar')->move('images/',$request->file('avatar')->getClientOriginalName());
        //     $data_akademik->avatar = $request->file('avatar')->getClientOriginalName();
        //     $data_akademik->save();
        // }
        return redirect('/dashboards')->with('sukses', 'Data berhasil diupdate');
    }

    public function delete($id){
        $data_akademik = \App\Akademik::find($id);
        $data_akademik->delete($data_akademik);
        return redirect('/dashboards')->with('sukses','Data berhasil dihapus');
    }

public function show(Request $request)
{

    
    if($request->has('cari')){
        $data_akademik = \App\Akademik::where('nama_lengkap','LIKE','%'.$request->cari.'%')->get();
    }else{
        
        $data_akademik = \App\Akademik::all();
    }
   

}

}
