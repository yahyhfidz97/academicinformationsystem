<?php

namespace App\Http\Controllers;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class GuruController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('cari')){
            $data_guru = \App\Guru::where('nama_lengkap','LIKE','%'.$request->cari.'%')->get();
        }else{
            $data_guru = \App\Guru::all();
            $data_akademik = \App\Akademik::select('tahun_akademik')->where('status', 1)->get();
        }
        
        return view('guru.index',['data_guru' => $data_guru, 'data_akademik' => $data_akademik]);
    }

    public function create(Request $request)
    {
        // $this->validate($request,[ 'nama_depan' => 'min:5',
        // 'nama_belakang' => 'required',
        //     'email' => 'required|email|unique:users',
        //     'jenis_kelamin' => 'required',
        //     'agama' => 'required',
        //     'avatar' => 'mimes:jpeg,png',
        //     ]);


        //insert ke tabel users
        $user = new \App\User;
        $user->role = $request->wali;
        $user->name = $request->nama_lengkap;
        $user->email = $request->email;
        $user->password = bcrypt('rahasia');
        $user->remember_token = Str::random(60);
        $user->save();

        //insert ke tabel guru
        $request->request->add(['user_id' => $user->id]);
        $guru = \App\Guru::create($request->all());
        if($request->hasFile('avatar')){
            $request->file('avatar')->move('images/',$request->file('avatar')->getClientOriginalName());
            $guru->avatar = $request->file('avatar')->getClientOriginalName();
            $guru->save();
        }
        return redirect('/guru')->with('sukses','Data berhasil ditambahkan');
    }

    public function edit($id)
    {
        $guru = \App\Guru::find($id);
        $data_akademik = \App\Akademik::all();
        return view('guru/edit',['guru' => $guru,'data_akademik' => $data_akademik]);
    }


    public function update(Request $request,$id)
    {
        //dd($request->all());
        $guru = \App\Guru::find($id);
        $guruall = \App\Guru::all();

        if($guru ->where(['wali'=>'Wali Kelas A'])){
            $guru->where(['wali'=>'Wali Kelas A'])->update(['wali'=>'Tidak ada']);
            
            $guru->wali = $request->wali;
            
            $guru->save();
        } else if($guru ->where(['wali'=>'Wali Kelas B'])){
            $guru ->where(['wali'=>'Wali Kelas B'])->update(['wali'=>'Tidak ada']);
            
            $guru->wali = $request->wali;

            $gurua->save();
        }
           
          
        

        // $guru ->where(['wali'=>'Wali Kelas A'])->update(['wali'=>'Tidak ada']);
        // $guru ->where(['wali'=>'Wali Kelas B'])->update(['wali'=>'Tidak ada']);
        //$guru ->where(['wali'=>'Wali Kelas B','wali'=>'Wali Kelas A'])->update(['wali'=>'Tidak ada']);
        //$guru ->where(function ($query) {
            //$query->where('wali', '=', 'Wali Kelas A')
                  //->orWhere('wali', '=', 'Wali Kelas B');
                //})->update(['wali'=>'Tidak ada']);
                // $guru->wali = $request->wali;
        //$guru->update($request->all());
        
        
        return redirect('/guru')->with('sukses', 'Data berhasil diupdate');
    }

    public function delete($id)
    {
        $guru = \App\Guru::find($id);
        $guru->delete($guru);
        return redirect('/guru')->with('sukses','Data berhasil dihapus');
    }

}
