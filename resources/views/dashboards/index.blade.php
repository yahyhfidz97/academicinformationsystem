@extends('layouts.master')
@section('header')
<style>body {padding:30px}
.print-area {border:1px solid white;padding:1em;margin:0 0 1em}</style>
@stop
@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <!-- TABLE HOVER -->
                    <div class="panel">
								<div class="panel-heading">
									<h2 class="panel-title">Tahun Akademik</h2>
                                   
                                </div>
								<div class="panel-body">
									<p>@foreach ($data_akademik as $item)
									<h2>T.A. {{$item->tahun_akademik}}</h2>
                                    
									@endforeach</p>
								</div>
							</div>
                        </div>

                        <div class="col-md-6">
                    <!-- TABLE HOVER -->
                    <div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title"><span class="label label-primary">Info</span></h3>
								</div>
								<div class="panel-body">
									<p><span>Jumlah Siswa:</span>
                                    {{$data_siswa->count()}}</p>
                                    
                                    <p><span>Jumlah Guru:</span>
                                    {{$data_guru->count()}}</p>
                                
                                    <p><span>Jumlah Rangkuman Kompetensi:</span>
                                    {{$data_rangkuman->count()}}</p>
                                    
                                    <p><span>Jumlah Indikator:</span>
                                    {{$data_indikator->count()}}</p>
								</div>

							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection    

        @section('footer')
        <script>
         function printDiv(elementId) {
        var a = document.getElementById('printing-css').value;
        var b = document.getElementById(elementId).innerHTML;
        window.frames["print_frame"].document.title = document.title;
        window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + b;
        window.frames["print_frame"].window.focus();
        window.frames["print_frame"].window.print();
        }
        
        </script>
        @stop
