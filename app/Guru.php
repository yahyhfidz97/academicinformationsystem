<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    protected $table = 'guru';
    protected $fillable = ['no_id_pegawai','nama_lengkap','tempat_lahir','tanggal_lahir','alamat','jenis_kelamin','pendidikan','wali','alamat','tahun_akademik','user_id'];
}
