@extends('layouts.master')

@section('header')
<!--untuk memanggil CSS untuk jquery edit-->
<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<!--untuk memberikan nama kelas pada tag yang akan dicetak-->
<style>
	body {
		padding: 30px
	}

	.print-area {
		border: 1px solid white;
		padding: 1em;
		margin: 0 0 1em
	}

	/* table {
    display: block;
    overflow-x: auto;
	
    white-space: nowrap; */

	/*css untuk tag wrap tabel*/
	/* .wrapper1,
	.wrapper2 {
		width: 560px;
		border: none 0px RED;
		overflow-x: scroll;
		overflow-y: hidden;
	}

	.wrapper1 {
		height: 20px;
	}

	.wrapper2 {
		height: 1500px;
	}

	.div1 {
		width: 2000px;
		height: 20px;
	}

	.div2 {
		width: 2000px;
		height: 1500px;
		background-color: lightgrey;
		overflow: auto;
	} */

	.table-header {
		position: relative;
		display: block;
	}

	.table-body {
		overflow: auto;
		height: 400px;
	}

	/* .sticky{
		min-width: 330px;
		height: 50px;
		position: relative;
		display: block;
	} */

	.header-cell {
		background-color: yellow;
		width: 130px;
		height: 50px;
	}

	.body-cell {
		min-width: 130px;
		height: 50px;
	}



</style>
@stop

@section('content')
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			@if(session('sukses'))
			<div class="alert alert-success" role="alert">{{session('sukses')}}
			</div>
			@endif

			@if(session('error'))
			<div class="alert alert-danger" role="alert">{{session('error')}}
			</div>
			@endif
			<div class="panel panel-profile">
				<div class="clearfix">
					<!-- LEFT COLUMN -->
					<div class="profile-left">
						<!-- PROFILE HEADER -->
						<div class="profile-header">
							<div class="overlay"></div>
							<div class="profile-main">
								<img src="{{$siswa->getAvatar()}}" class="img-circle" alt="Avatar" style="width: 100px; height: 100px;">
								<h3 class="name">{{$siswa->nama_lengkap}}</h3>
								<!--<span class="online-status status-available">Available</span>-->
							</div>
							<div class="profile-stat">
								<div class="row">
									<div class="col-md-4 stat-item">
										{{$siswa->rangkuman->count()}} <span>Poin Penilaian Siswa</span>
									</div>
									<div class="col-md-4 stat-item">

									</div>
									<div class="col-md-4 stat-item">
										{{$siswa->indikator->count()}} <span>Poin Indikator</span>
									</div>
								</div>
							</div>
						</div>
						<!-- END PROFILE HEADER -->
						<!-- PROFILE DETAIL -->
						<div class="profile-detail">
							<div class="profile-info">
								<h4 class="heading">Data diri</h4>
								<ul class="list-unstyled list-justify">
									<li>Nomor Induk : <span>{{$siswa->no_idk}}</span></li>
									<li>Nama Lengkap : <span>{{$siswa->nama_lengkap}}</span></li>
									<li>Jenis Kelamin : <span>{{$siswa->jenis_kelamin}}</span></li>
									<li>Kelas : <span>{{$siswa->kelas}}</span></li>
									<li>Tahun Akademik : <span>@foreach ($data_akademik as $item)
											{{$item->tahun_akademik}}
											@endforeach</span></li>
									<li>Alamat : <span>{{$siswa->alamat}}</span></li>
								</ul>
							</div>
							@if(auth()->user()->role == 'admin')
							<div class="text-center"><a href="/siswa/{{$siswa->id}}/edit" class="btn btn-warning">Edit Profile</a></div>
							@endif
						</div>
						<!-- END PROFILE DETAIL -->
					</div>
					<!-- END LEFT COLUMN -->
					<!-- RIGHT COLUMN -->
					<div class="profile-right">
						<!-- Tabel Rangkuman Penilaian Siswa -->
						@if(auth()->user()->role == 'admin')
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Tambah Nilai
						</button>
						@endif
						@if(auth()->user()->role == 'admin' OR 'guru' OR 'guru2')
						<button type="button" class="btn no-print" onclick="javascript:printDiv('print-area-2');"><i class="lnr lnr-printer"></i></button>
						@endif

						<div class="panel">
							<div class="panel-heading">
								<h3 class="panel-title">Rangkuman Penilaian Siswa</h3>
							</div>

							<div><small>
									<h7>Penilaian capaian perkembangan anak : </h7>
									<ul>
										<li>(BB) artinya Belum Berkembang: bila anak melakukannya harus dengan bimbingan atau dicontohkan oleh guru.</li>
										<li>(MB) artinya Mulai Berkembang: bila anak melakukannya masih harus diingatkan atau dibantu oleh guru.</li>
										<li>(BSH) artinya Berkembang Sesuai Harapan: bila anak sudah dapat melakukannya secara mandiri dan konsisten tanpa harus diingatkan atau dicontohkan oleh guru.</li>
										<li>(BSB) artinya Berkembang Sangat Baik: bila anak sudah dapat melakukannya secara mandiri dan sudah dapat membantu temannya yang belum mencapai kemampuan sesuai indikator yang diharapkan.</li>
										<li><b>Penilaian dengan skala perminggu selama 1 semester</b></li>
									</ul>
								</small></div>


							<div class="panel-body print-area" id="print-area-2">
								
								<h3 class="panel-title">Rangkuman Penilaian Siswa</h3>
								<li>Nomor Induk : <span>{{$siswa->no_idk}}</span></li>
								<li>Nama Lengkap : <span>{{$siswa->nama_lengkap}}</span></li>
								<li>Jenis Kelamin : <span>{{$siswa->jenis_kelamin}}</span></li>
								<li>Kelas : <span>{{$siswa->kelas}}</span></li>
								<li>Tahun Akademik : <span>@foreach ($data_akademik as $item)
										{{$item->tahun_akademik}}
										@endforeach</span></li>
								<!--tag scrollable-->
								<!-- <div class="wrapper1">
									<div class="div1">
									</div>
								</div>
								<div class="wrapper2">
									<div class="div2"> -->

								<div class="table-body">
									<table class="table table-striped" border="1">
										<thead class="table-header">
											<tr>
												<th class="header-cell col1">KODE</th>
												<th class="header-cell col1">NAMA</th>
												<th class="header-cell col1">1</th>
												<th class="header-cell col1">2</th>
												<th class="header-cell col1">3</th>
												<th class="header-cell col1">4</th>
												<th class="header-cell col1">5</th>
												<th class="header-cell col1">6</th>
												<th class="header-cell col1">7</th>
												<th class="header-cell col1">8</th>
												<th class="header-cell col1">9</th>
												<th class="header-cell col1">10</th>
												<th class="header-cell col1">11</th>
												<th class="header-cell col1">12</th>
												<th class="header-cell col1">13</th>
												<th class="header-cell col1">14</th>
												<th class="header-cell col1">15</th>
												<th class="header-cell col1">16</th>
												<th class="header-cell col1">17</th>



												@if(auth()->user()->role == 'admin')
												<th class="header-cell col1">AKSI</th>
												@endif
											</tr>
										</thead>
										<tbody style="display: block;">
											@foreach($siswa->rangkuman as $mapel)
											<tr>
												<td class="body-cell col1">{{$mapel->kode}}</td>
												<td class="body-cell col1">{{$mapel->nama}}</td>

												<td class="body-cell col1">@if(auth()->user()->role == 'admin' OR 'guru')<a href="#" class="nilai" data-type="text" data-pk="{{$mapel->id}}" data-url="/api/siswa/{{$siswa->id}}/editnilai" data-title="Masukkan Nilai">
														@endif{{$mapel->pivot->nilai}}</a></td>

												<td class="body-cell col1">@if(auth()->user()->role == 'admin' OR 'guru')<a href="#" class="nilai2" data-type="text" data-pk="{{$mapel->id}}" data-url="/api/siswa/{{$siswa->id}}/editnilaiw2" data-title="Masukkan Nilai">
														@endif{{$mapel->pivot->nilai2}}</a></td>

												<td class="body-cell col1">@if(auth()->user()->role == 'admin' OR 'guru')<a href="#" class="nilai3" data-type="text" data-pk="{{$mapel->id}}" data-url="/api/siswa/{{$siswa->id}}/editnilaiw3" data-title="Masukkan Nilai">
														@endif{{$mapel->pivot->nilai3}}</a></td>

												<td class="body-cell col1">@if(auth()->user()->role == 'admin' OR 'guru')<a href="#" class="nilai4" data-type="text" data-pk="{{$mapel->id}}" data-url="/api/siswa/{{$siswa->id}}/editnilaiw4" data-title="Masukkan Nilai">
														@endif{{$mapel->pivot->nilai4}}</a></td>

												<td class="body-cell col1">@if(auth()->user()->role == 'admin' OR 'guru')<a href="#" class="nilai5" data-type="text" data-pk="{{$mapel->id}}" data-url="/api/siswa/{{$siswa->id}}/editnilaiw5" data-title="Masukkan Nilai">
														@endif{{$mapel->pivot->nilai5}}</a></td>

												<td class="body-cell col1">@if(auth()->user()->role == 'admin' OR 'guru')<a href="#" class="nilai6" data-type="text" data-pk="{{$mapel->id}}" data-url="/api/siswa/{{$siswa->id}}/editnilaiw6" data-title="Masukkan Nilai">
														@endif{{$mapel->pivot->nilai6}}</a></td>

												<td class="body-cell col1">@if(auth()->user()->role == 'admin' OR 'guru')<a href="#" class="nilai7" data-type="text" data-pk="{{$mapel->id}}" data-url="/api/siswa/{{$siswa->id}}/editnilaiw7" data-title="Masukkan Nilai">
														@endif{{$mapel->pivot->nilai7}}</a></td>

												<td class="body-cell col1">@if(auth()->user()->role == 'admin' OR 'guru')<a href="#" class="nilai8" data-type="text" data-pk="{{$mapel->id}}" data-url="/api/siswa/{{$siswa->id}}/editnilaiw8" data-title="Masukkan Nilai">
														@endif{{$mapel->pivot->nilai8}}</a></td>

												<td class="body-cell col1">@if(auth()->user()->role == 'admin' OR 'guru')<a href="#" class="nilai9" data-type="text" data-pk="{{$mapel->id}}" data-url="/api/siswa/{{$siswa->id}}/editnilaiw9" data-title="Masukkan Nilai">
														@endif{{$mapel->pivot->nilai9}}</a></td>

												<td class="body-cell col1">@if(auth()->user()->role == 'admin' OR 'guru')<a href="#" class="nilai10" data-type="text" data-pk="{{$mapel->id}}" data-url="/api/siswa/{{$siswa->id}}/editnilaiw10" data-title="Masukkan Nilai">
														@endif{{$mapel->pivot->nilai10}}</a></td>

												<td class="body-cell col1">@if(auth()->user()->role == 'admin' OR 'guru')<a href="#" class="nilai11" data-type="text" data-pk="{{$mapel->id}}" data-url="/api/siswa/{{$siswa->id}}/editnilaiw11" data-title="Masukkan Nilai">
														@endif{{$mapel->pivot->nilai11}}</a></td>

												<td class="body-cell col1">@if(auth()->user()->role == 'admin' OR 'guru')<a href="#" class="nilai12" data-type="text" data-pk="{{$mapel->id}}" data-url="/api/siswa/{{$siswa->id}}/editnilaiw12" data-title="Masukkan Nilai">
														@endif{{$mapel->pivot->nilai12}}</a></td>

												<td class="body-cell col1">@if(auth()->user()->role == 'admin' OR 'guru')<a href="#" class="nilai13" data-type="text" data-pk="{{$mapel->id}}" data-url="/api/siswa/{{$siswa->id}}/editnilaiw13" data-title="Masukkan Nilai">
														@endif{{$mapel->pivot->nilai13}}</a></td>

												<td class="body-cell col1">@if(auth()->user()->role == 'admin' OR 'guru')<a href="#" class="nilai14" data-type="text" data-pk="{{$mapel->id}}" data-url="/api/siswa/{{$siswa->id}}/editnilaiw14" data-title="Masukkan Nilai">
														@endif{{$mapel->pivot->nilai14}}</a></td>

												<td class="body-cell col1">@if(auth()->user()->role == 'admin' OR 'guru')<a href="#" class="nilai15" data-type="text" data-pk="{{$mapel->id}}" data-url="/api/siswa/{{$siswa->id}}/editnilaiw15" data-title="Masukkan Nilai">
														@endif{{$mapel->pivot->nilai15}}</a></td>

												<td class="body-cell col1">@if(auth()->user()->role == 'admin' OR 'guru')<a href="#" class="nilai16" data-type="text" data-pk="{{$mapel->id}}" data-url="/api/siswa/{{$siswa->id}}/editnilaiw16" data-title="Masukkan Nilai">
														@endif{{$mapel->pivot->nilai16}}</a></td>

												<td class="body-cell col1">@if(auth()->user()->role == 'admin' OR 'guru')<a href="#" class="nilai17" data-type="text" data-pk="{{$mapel->id}}" data-url="/api/siswa/{{$siswa->id}}/editnilaiw17" data-title="Masukkan Nilai">
														@endif{{$mapel->pivot->nilai17}}</a></td>




												@if(auth()->user()->role == 'admin')
												<td class="body-cell col1">
													<a href="/siswa/{{$siswa->id}}/{{$mapel->id}}/deletenilai" class="btn btn-danger btn-sm" onClick="return confirm('Yakin mau dihapus?')">Delete</a>
												</td>
												@endif
											</tr>
											@endforeach
										</tbody>


									</table>
								</div>
								<!--tag scrollable-->
								<!-- </div>
								</div> -->

							</div>
						</div>



						<!-- Tabel Indikator -->
						@if(auth()->user()->role == 'admin')
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2">Tambah Indikator</button>
						@endif

						@if(auth()->user()->role == 'admin' OR 'guru' OR 'guru2')
						<button type="button" class="btn no-print" onclick="javascript:printDiv('print-area-3');"><i class="lnr lnr-printer"></i></button>
						@endif

						<div class="panel">
							<div class="panel-heading">
								<h3 class="panel-title">Laporan Perkembangan Siswa</h3>
							</div>
							<div class="panel-body print-area" id="print-area-3">
								<h3 class="panel-title">Laporan Perkembangan Siswa</h3>
								<li>Nomor Induk : <span>{{$siswa->no_idk}}</span></li>
								<li>Nama Lengkap : <span>{{$siswa->nama_lengkap}}</span></li>
								<li>Jenis Kelamin : <span>{{$siswa->jenis_kelamin}}</span></li>
								<li>Kelas : <span>{{$siswa->kelas}}</span></li>
								<li>Tahun Akademik : <span>@foreach ($data_akademik as $item)
										{{$item->tahun_akademik}}
										@endforeach</span></li>
								<table class="table table-striped" border="1">
									<thead>
										<tr>
											<th>KODE</th>
											<th>NAMA</th>

											<th>ISI INDIKATOR</th>
											@if(auth()->user()->role == 'admin')
											<th>AKSI</th>
											@endif
										</tr>
									</thead>
									<tbody>
										@foreach($siswa->indikator as $indikator)
										<tr>
											<td>{{$indikator->kode}}</td>
											<td>{{$indikator->nama_indikator}}</td>

											<td>@if(auth()->user()->role == 'admin' OR 'guru' OR 'guru2')<a href="#" class="isi_indikator" data-type="textarea" data-pk="{{$indikator->id}}" data-url="/api/siswa/{{$siswa->id}}/editindikator" data-title="Masukkan Isi Indikator">@endif{{$indikator->pivot->isi_indikator}}</a></td>

											@if(auth()->user()->role == 'admin')
											<td>
												<a href="/siswa/{{$siswa->id}}/{{$indikator->id}}/deleteindikator" class="btn btn-danger btn-sm" onClick="return confirm('Yakin mau dihapus?')">Delete</a>
											</td>
											@endif
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>
				<!-- END RIGHT COLUMN -->
			</div>
		</div>
	</div>
</div>
<!-- END MAIN CONTENT -->
</div>

<textarea id="printing-css" style="display:none;">html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:before,blockquote:after,q:before,q:after{content:'';content:none}table{border-collapse:collapse;border-spacing:0}body{font:normal normal .8125em/1.4 Arial,Sans-Serif;background-color:white;color:#333}strong,b{font-weight:bold}cite,em,i{font-style:italic}a{text-decoration:none}a:hover{text-decoration:underline}a img{border:none}abbr,acronym{border-bottom:1px dotted;cursor:help}sup,sub{vertical-align:baseline;position:relative;top:-.4em;font-size:86%}sub{top:.4em}small{font-size:86%}kbd{font-size:80%;border:1px solid #999;padding:2px 5px;border-bottom-width:2px;border-radius:3px}mark{background-color:#ffce00;color:black}p,blockquote,pre,table,figure,hr,form,ol,ul,dl{margin:1.5em 0}hr{height:1px;border:none;background-color:#666}h1,h2,h3,h4,h5,h6{font-weight:bold;line-height:normal;margin:1.5em 0 0}h1{font-size:200%}h2{font-size:180%}h3{font-size:160%}h4{font-size:140%}h5{font-size:120%}h6{font-size:100%}ol,ul,dl{margin-left:3em}ol{list-style:decimal outside}ul{list-style:disc outside}li{margin:.5em 0}dt{font-weight:bold}dd{margin:0 0 .5em 2em}input,button,select,textarea{font:inherit;font-size:100%;line-height:normal;vertical-align:baseline}textarea{display:block;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}pre,code{font-family:"Courier New",Courier,Monospace;color:inherit}pre{white-space:pre;word-wrap:normal;overflow:auto}blockquote{margin-left:2em;margin-right:2em;border-left:4px solid #ccc;padding-left:1em;font-style:italic}table[border="1"] th,table[border="1"] td,table[border="1"] caption{border:1px solid;padding:.5em 1em;text-align:left;vertical-align:top}th{font-weight:bold;}table[border="1"] caption{border:none;font-style:italic}.no-print{display:none} 
.table-header {
		display: block;
	}  .header-cell {
		background-color: yellow;
		width: 100px;
		height: 20px;
	}

	.body-cell {
		width: 100px;
		height: 20px;
	}

 </textarea>
<iframe id="printing-frame" name="print_frame" src="about:blank" style="display:none;"></iframe>

<!-- Modal Rankuman Penilaian -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Nilai</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="/siswa/{{$siswa->id}}/addnilai" method="POST" enctype="multipart/form-data">
					{{csrf_field()}}
					<div class="form-group">
						<label for="rangkuman">Rangkuman Kompetensi</label>
						<select class="form-control" id="rangkuman" name="rangkuman">
							@foreach($matapelajaran as $mp)
							<option value="{{$mp->id}}">{{$mp->nama}}</option>
							@endforeach
						</select>
					</div>

					<div class="mb-3 {{$errors->has('nilai') ? ' has-error' : ''}}">
						<label for="exampleInputEmail1" class="form-label">Nilai</label>
						<input name="nilai" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nilai" value="{{old('nilai')}}">
						@if($errors->has('nilai'))
						<span class="help-block">{{$errors->first('nilai')}}</span>
						@endif
					</div>

			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Simpan</button>
				</form>
			</div>
		</div>
	</div>
</div>



<!-- Modal Laporan Perkembangan Siswa -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Indikator</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="/siswa/{{$siswa->id}}/addindikator" method="POST" enctype="multipart/form-data">
					{{csrf_field()}}


					<div class="form-group">
						<label for="indikator">Nama Indikator</label>
						<select class="form-control" id="indikator" name="indikator">
							@foreach($indikatorisi as $idk)
							<option value="{{$idk->id}}">{{$idk->nama_indikator}}</option>
							@endforeach
						</select>
					</div>

					<div class="mb-3 {{$errors->has('nama_depan') ? ' has-error' : ''}}">
						<label for="exampleInputEmail1" class="form-label">Isi Indikator</label>
						<textarea name="isi_indikator" type="textarea" class="form-control" id="floatingTextarea" aria-describedby="emailHelp" placeholder="Isi Indikator" value="{{old('isi_indikator')}}"></textarea>
						@if($errors->has('isi_indikator'))
						<span class="help-block">{{$errors->first('isi_indikator')}}</span>
						@endif
					</div>



			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Simpan</button>
				</form>
			</div>
		</div>
	</div>
</div>
@stop

@section('footer')
<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<!--jquery untuk edit nilai-->

@if(auth()->user()->role == 'admin' OR 'guru')
<script>
	$(document).ready(function() {
		$('.nilai').editable();
	});
</script>
@endif

<script>
	$(document).ready(function() {
		$('.nilai2').editable();
	});
</script>

<script>
	$(document).ready(function() {
		$('.nilai3').editable();
	});
</script>

<script>
	$(document).ready(function() {
		$('.nilai4').editable();
	});
</script>

<script>
	$(document).ready(function() {
		$('.nilai5').editable();
	});
</script>

<script>
	$(document).ready(function() {
		$('.nilai6').editable();
	});
</script>

<script>
	$(document).ready(function() {
		$('.nilai7').editable();
	});
</script>

<script>
	$(document).ready(function() {
		$('.nilai8').editable();
	});
</script>

<script>
	$(document).ready(function() {
		$('.nilai9').editable();
	});
</script>

<script>
	$(document).ready(function() {
		$('.nilai10').editable();
	});
</script>

<script>
	$(document).ready(function() {
		$('.nilai11').editable();
	});
</script>

<script>
	$(document).ready(function() {
		$('.nilai12').editable();
	});
</script>

<script>
	$(document).ready(function() {
		$('.nilai13').editable();
	});
</script>

<script>
	$(document).ready(function() {
		$('.nilai14').editable();
	});
</script>

<script>
	$(document).ready(function() {
		$('.nilai15').editable();
	});
</script>

<script>
	$(document).ready(function() {
		$('.nilai16').editable();
	});
</script>

<script>
	$(document).ready(function() {
		$('.nilai17').editable();
	});
</script>





<!--jquery untuk edit isi indikator-->
<script>
	$(document).ready(function() {
		$('.isi_indikator').editable();
	});
</script>

<!--fungsi untuk cetak tabel-->
<script>
	function printDiv(elementId) {
		var a = document.getElementById('printing-css').value;
		var b = document.getElementById(elementId).innerHTML;
		window.frames["print_frame"].document.title = document.title;
		window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + b;
		window.frames["print_frame"].window.focus();
		window.frames["print_frame"].window.print();
	}
</script>

<!--fungsi untuk wrap tabel dengan scroll bar-->
<!-- <script>
	$(function() {
		$(".wrapper1").scroll(function() {
			$(".wrapper2")
				.scrollLeft($(".wrapper1").scrollLeft());
		});
		$(".wrapper2").scroll(function() {
			$(".wrapper1")
				.scrollLeft($(".wrapper2").scrollLeft());
		});
	});
</script> -->

<!-- js untuk wrapping table + scrollbar -->
<script>
	$(".table-body").scroll(function() {
		console.log(this.scrollTop);
		$(".table-header")[0].style.top = (this.scrollTop - 5) + 'px';
		
	});
</script>

@stop