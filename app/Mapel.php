<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mapel extends Model
{
    protected $table ='rangkuman';
    protected $fillable = ['kode','nama','kategori','guru_komp','semester'];

    public function siswa()
    {
        return $this->belongsToMany(Siswa::class)->withPivot(['nilai'])->withTimestamps();
    }
}
