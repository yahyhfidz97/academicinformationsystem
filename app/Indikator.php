<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Indikator extends Model
{
    protected $table = 'indikator';
    protected $fillable = ['kode','nama_indikator','semester'];

    public function siswa(){
        return $this->belongsToMany(Siswa::class)->withPivot(['isi_indikator']);
    }
}
