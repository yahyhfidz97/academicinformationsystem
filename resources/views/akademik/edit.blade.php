@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
							<div class="panel-heading">
							<h3 class="panel-title">Inputs</h3>
							</div>
							<div class="panel-body">
                            <form action="/akademik/{{$editakademik->id}}/update" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}

                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Tahun Akademik</label>
                            <input name="tahun_akademik" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tahun Akademik" value="{{$editakademik->tahun_akademik}}">
                        </div>

                       

       

                       
                        
                        <button type="submit" class="btn btn-warning">Update</button>
                    </form>

						</div>
				    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop