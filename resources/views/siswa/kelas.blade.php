@extends('layouts.master')
@section('header')

@stop
@section('content')
<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
				@if(session('sukses'))
				<div class="alert alert-success" role="alert">{{session('sukses')}}
				</div>
				@endif

				@if(session('error'))
				<div class="alert alert-danger" role="alert">{{session('error')}}
				</div>
				@endif
					<div class="panel panel-profile">
						<div class="clearfix">
							<!-- LEFT COLUMN -->
							<div class="profile-left">
								<!-- PROFILE HEADER -->
								<!-- <div class="profile-header">
									<div class="overlay"></div>
									<div class="profile-main">
										<img src="#" class="img-circle" alt="Avatar"
										style="width: 100px; height: 100px;">
										<h3 class="name">ini A</h3>
										<span class="online-status status-available">Available</span>
									</div>
									<div class="profile-stat">
										<div class="row">
											<div class="col-md-4 stat-item">
											{{$data_siswa->count()}} <span>Jumlah Siswa</span>
											</div>
											<div class="col-md-4 stat-item">
											
											</div>
											<div class="col-md-4 stat-item">
											ini zonk <span>Jumlah Kompetensi</span>
											</div>
										</div>
									</div>
								</div> -->
								<!-- END PROFILE HEADER -->
								<!-- PROFILE DETAIL -->
								<div class="profile-detail">
									<div class="profile-info">
										<h4 class="heading">Kelas A</h4>
										<ul class="list-unstyled list-justify">
											<li>Jumlah Siswa : <span>{{$data_siswa->count()}}</span></li>
											<li>Jumlah Kompetensi : <span>{{$data_mapel->count()}}</span></li>
											<li>Tahun Akademik : <span>@foreach ($data_akademik as $item)
																		{{$item->tahun_akademik}}
																		@endforeach</span></li>
											
										</ul>
									</div>
									@if(auth()->user()->role == 'admin' OR 'guru')
									<!-- <div class="text-center"><a href="#" class="btn btn-warning">Edit Profile</a></div> -->
									@endif
								</div>

								<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Siswa</h3>
								</div>
								<div class="panel-body print-area" id="print-area-3">
									<table class="table table-striped" border="1">
										<thead>
											<tr>
												<th>NO. INDUK</th>
												<th>NAMA</th>
												<th>KELAS</th>
											</tr>
										</thead>
										<tbody>
										@foreach($data_siswa as $siswa)
											<tr>
											<td><a href="/siswa/{{$siswa->id}}/profile">{{$siswa->no_idk}}</a></td>
											
												<td>{{$siswa->nama_lengkap}}</td>
												<td>{{$siswa->kelas}}</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>





								<!-- END PROFILE DETAIL -->
							</div>
							<!-- END LEFT COLUMN -->
							<!-- RIGHT COLUMN -->
							<div class="profile-right">
							<!-- Tabel Rangkuman Penilaian Siswa -->
							@if(auth()->user()->role == 'admin' OR 'guru')
							@endif

							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Wali Kelas</h3>
								</div>
								<div class="panel-body print-area" id="print-area-2">
									<table class="table table-striped" border="1">
										<thead>
											<tr>
											@if(auth()->user()->role == 'guru')
												<th>NAMA</th>
											@endif
											@if(auth()->user()->role == 'admin')
												<th>NAMA</th>
											@endif
											
											</tr>
										</thead>
										<tbody>
										@foreach($data_guru as $guru)
											<tr>
											@if(auth()->user()->role == 'guru')
												<td>{{auth()->user()->name}}</td>
												@endif

												@if(auth()->user()->role == 'admin')
												<td>{{$guru->nama_lengkap}} </td>
												@endif
												
												

											</tr>
											@endforeach
										</tbody>
									<!-- <div>
										<small>
										<h7>Penilaian capaian perkembangan anak : </h7>
										<ul>
  										<li>(BB) artinya Belum Berkembang: bila anak melakukannya harus dengan bimbingan atau dicontohkan oleh guru.</li>
  										<li>(MB) artinya Mulai Berkembang: bila anak melakukannya masih harus diingatkan atau dibantu oleh guru.</li>
										<li>(BSH) artinya Berkembang Sesuai Harapan: bila anak sudah dapat melakukannya secara mandiri dan konsisten tanpa harus diingatkan atau dicontohkan oleh guru.</li>
										<li>(BSB) artinya Berkembang Sangat Baik: bila anak sudah dapat melakukannya secara mandiri dan sudah dapat membantu temannya yang belum mencapai kemampuan sesuai indikator yang diharapkan.</li>
										</ul>  
										</small>
									</div> -->
									</table>
								</div>
							</div>



							<!-- Tabel Indikator -->
							@if(auth()->user()->role == 'admin' OR 'guru')
							<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2">Tambah Indikator</button>
							<button type="button" class="btn no-print" onclick="javascript:printDiv('print-area-3');"><i class="lnr lnr-printer"></i></button> -->
							@endif
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Rangkuman Kompetensi</h3>
								</div>
								<div class="panel-body print-area" id="print-area-3">
									<table class="table table-striped" border="1">
										<thead>
											<tr>
												<th>KODE</th>
												<th>NAMA</th>
											</tr>
										</thead>
										<tbody>
										@foreach($data_mapel as $mapel)
											<tr>
											<td><a href="/rangkuman/{{$mapel->id}}/penilaian">{{$mapel->kode}}</a></td>
												<td>{{$mapel->nama}}</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
							
							</div>
							<!-- END RIGHT COLUMN -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>

		 <textarea id="printing-css" style="display:none;">html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:before,blockquote:after,q:before,q:after{content:'';content:none}table{border-collapse:collapse;border-spacing:0}body{font:normal normal .8125em/1.4 Arial,Sans-Serif;background-color:white;color:#333}strong,b{font-weight:bold}cite,em,i{font-style:italic}a{text-decoration:none}a:hover{text-decoration:underline}a img{border:none}abbr,acronym{border-bottom:1px dotted;cursor:help}sup,sub{vertical-align:baseline;position:relative;top:-.4em;font-size:86%}sub{top:.4em}small{font-size:86%}kbd{font-size:80%;border:1px solid #999;padding:2px 5px;border-bottom-width:2px;border-radius:3px}mark{background-color:#ffce00;color:black}p,blockquote,pre,table,figure,hr,form,ol,ul,dl{margin:1.5em 0}hr{height:1px;border:none;background-color:#666}h1,h2,h3,h4,h5,h6{font-weight:bold;line-height:normal;margin:1.5em 0 0}h1{font-size:200%}h2{font-size:180%}h3{font-size:160%}h4{font-size:140%}h5{font-size:120%}h6{font-size:100%}ol,ul,dl{margin-left:3em}ol{list-style:decimal outside}ul{list-style:disc outside}li{margin:.5em 0}dt{font-weight:bold}dd{margin:0 0 .5em 2em}input,button,select,textarea{font:inherit;font-size:100%;line-height:normal;vertical-align:baseline}textarea{display:block;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}pre,code{font-family:"Courier New",Courier,Monospace;color:inherit}pre{white-space:pre;word-wrap:normal;overflow:auto}blockquote{margin-left:2em;margin-right:2em;border-left:4px solid #ccc;padding-left:1em;font-style:italic}table[border="1"] th,table[border="1"] td,table[border="1"] caption{border:1px solid;padding:.5em 1em;text-align:left;vertical-align:top}th{font-weight:bold}table[border="1"] caption{border:none;font-style:italic}.no-print{display:none}</textarea>
                                <iframe id="printing-frame" name="print_frame" src="about:blank" style="display:none;"></iframe>

        @endsection    

@section('footer')


@stop