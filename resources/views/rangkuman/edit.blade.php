@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Inputs</h3>
								</div>
								<div class="panel-body">
                                <form action="/rangkuman/{{$rangkuman->id}}/update" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}

                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Kode</label>
                            <input name="kode" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Kode" value="{{$rangkuman->kode}}">
                        </div>

                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Nama</label>
                            <input name="nama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama" value="{{$rangkuman->nama}}">
                        </div>

        <label for="kategori" class="form-label">Kategori</label>
        <select id="kategori" class="form-control" name="kategori">
        <option selected value="NILAI – NILAI AGAMA DAN MORAL" @if ($rangkuman -> kategori == 'NILAI – NILAI AGAMA DAN MORAL') selected @endif >NILAI – NILAI AGAMA DAN MORAL</option>
        <option value="FISIK MOTORIK" @if ($rangkuman -> kategori == 'FISIK MOTORIK') selected @endif >FISIK MOTORIK</option>
        <option value="KOGNITIF" @if ($rangkuman -> kategori == 'KOGNITIF') selected @endif >KOGNITIF</option>
        <option value="BAHASA" @if ($rangkuman -> kategori == 'BAHASA') selected @endif >BAHASA</option>
        <option value="SOSIAL EMOSIONAL" @if ($rangkuman -> kategori == 'SOSIAL EMOSIONAL') selected @endif >SOSIAL EMOSIONAL</option>
        <option value="SENI" @if ($rangkuman -> kategori == 'SENI') selected @endif >SENI</option>
        </select>
        
        


        

        <div class="mb-3">
		<label for="akademik">Tahun Akademik</label>
		<select class="form-control" id="akademik" name="akademik">
		    @foreach($akademikisi as $akd)
			<option value="{{$akd->id}}">{{$akd->tahun_akademik}}</option>
			@endforeach
		</select>
		</div>
                        
                        <button type="submit" class="btn btn-warning">Update</button>
                    </form>

						</div>
				    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop