<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Akademik extends Model
{
    protected $table = 'akademik';
    protected $fillable = ['tahun_akademik'];
    public function siswa()
    {
        return $this->belongsToMany(Siswa::class)->withTimestamps();
    }

    public function rangkuman()
    {
        return $this->belongsToMany(Rangkuman::class)->withTimestamps();
    }
}
