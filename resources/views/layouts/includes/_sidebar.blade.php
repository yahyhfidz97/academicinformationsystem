<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<li><a href="/dashboards" class="active"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
						@if(auth()->user()->role == 'admin')
						<li><a href="/akademik" class=""><i class="lnr lnr-user"></i> <span>Tahun Akademik</span></a></li>
						<li><a href="/siswa" class=""><i class="lnr lnr-user"></i> <span>Siswa</span></a></li>

						<li>
							<a href="/siswa/kelas" data-toggle="collapse" class="active" aria-expanded="true"><i class="lnr lnr-file-empty"></i> <span>Kelas</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPages" class="collapse in" aria-expanded="true" style="">
								<ul class="nav">
									<li><a href="/siswa/kelas" class="active">Kelas A</a></li>
									<li><a href="/siswa/kelas2" class="">Kelas B</a></li>
								</ul>
							</div>
						</li>


						<li><a href="/guru" class=""><i class="lnr lnr-user"></i> <span>Guru</span></a></li>
						<li><a href="/rangkuman" class=""><i class="lnr lnr-user"></i> <span>Rangkuman Kompetensi </span></a></li>
						<li><a href="/indikator" class=""><i class="lnr lnr-user"></i> <span>Indikator Raport</span></a></li>
						@endif
						@if(auth()->user()->role == 'guru')
						
						<li>
							<a href="/siswa/kelas" data-toggle="collapse" class="active" aria-expanded="true"><i class="lnr lnr-file-empty"></i> <span>Kelas</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPages" class="collapse in" aria-expanded="true" style="">
								<ul class="nav">
									<li><a href="/siswa/kelas" class="active">Kelas A</a></li>
									
								</ul>
							</div>
						</li>
						<li><a href="/rangkuman" class=""><i class="lnr lnr-user"></i> <span>Rangkuman Kompetensi </span></a></li>
						<li><a href="/indikator" class=""><i class="lnr lnr-user"></i> <span>Indikator Raport</span></a></li>
						@endif

						@if(auth()->user()->role == 'guru2')
						
						<li>
							<a href="/siswa/kelas" data-toggle="collapse" class="active" aria-expanded="true"><i class="lnr lnr-file-empty"></i> <span>Kelas</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPages" class="collapse in" aria-expanded="true" style="">
								<ul class="nav">
									
									<li><a href="/siswa/kelas2" class="">Kelas B</a></li>
								</ul>
							</div>
						</li>
						<li><a href="/rangkuman" class=""><i class="lnr lnr-user"></i> <span>Rangkuman Kompetensi </span></a></li>
						<li><a href="/indikator" class=""><i class="lnr lnr-user"></i> <span>Indikator Raport</span></a></li>
						@endif

						@if(auth()->user()->role == 'siswa')
						<li><a href="/siswa" class=""><i class="lnr lnr-user"></i> <span>Siswa</span></a></li>
						@endif
					</ul>
				</nav>
			</div>
		</div>