<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class RangkumanController extends Controller
{
    public function index(Request $request)
    {
        
            //$data_rangkuman = \App\Rangkuman::all();
            $data_rangkuman = DB::table('akademik_rangkuman')
            ->join('akademik', 'akademik_rangkuman.akademik_id', '=', 'akademik.id')
            ->join('rangkuman', 'rangkuman.id', '=', 'akademik_rangkuman.rangkuman_id')
            ->select('rangkuman.id','rangkuman.kode','rangkuman.nama','rangkuman.kategori','rangkuman.guru_komp')
            ->where('status',1)
            ->get();
            $data_akademik = \App\Akademik::select('tahun_akademik')->where('status', 1)->get();
            $data_guru = \App\Guru::all();
        
        
        return view('rangkuman.index',['data_rangkuman' => $data_rangkuman, 'data_akademik' => $data_akademik,'data_guru' => $data_guru]);
    }

    public function index2(Request $request)
    {
            $data_guru = \App\Guru::all();
            $data_rangkuman = \App\Rangkuman::all();
            $data_akademik = \App\Akademik::select('tahun_akademik')->where('status', 1)->get();
        
        
            return view('rangkuman.index',['data_rangkuman' => $data_rangkuman, 'data_akademik' => $data_akademik,'data_guru' => $data_guru]);
    }


    public function create(Request $request)
    {
        // $this->validate($request,[ 'nama_depan' => 'min:5',
        // 'nama_belakang' => 'required',
        //     'email' => 'required|email|unique:users',
        //     'jenis_kelamin' => 'required',
        //     'agama' => 'required',
        //     'avatar' => 'mimes:jpeg,png',
        //     ]);


        //insert ke tabel users
        // $user = new \App\User;
        // $user->role = 'guru';
        // $user->name = $request->no_id_pegawai;
        // $user->email = $request->email;
        // $user->password = bcrypt('rahasia');
        // $user->remember_token = Str::random(60);
        // $user->save();

        //insert ke tabel guru
        //$request->request->add(['user_id' => $user->id]);
       
        $rangkuman = \App\Rangkuman::create($request->all());
        if($request->hasFile('avatar')){
            $request->file('avatar')->move('images/',$request->file('avatar')->getClientOriginalName());
            $rangkuman->avatar = $request->file('avatar')->getClientOriginalName();
            $rangkuman->save();
        }
        return redirect('/rangkuman')->with('sukses','Data berhasil ditambahkan');
    }

    public function edit($id)
    {
        $rangkuman = \App\Rangkuman::find($id);
        $data_akademik = \App\Akademik::select('tahun_akademik')->where('status', 1)->get();
        $akademikisi = \App\Akademik::select('id','tahun_akademik','status')->where('status', 1)->get();
        $data_guru = \App\Guru::all();
        return view('rangkuman/edit',['rangkuman' => $rangkuman, 'data_akademik' => $data_akademik, 'akademikisi' => $akademikisi,'data_guru' => $data_guru]);
    }

    public function update(Request $request,$id)
    {
        //dd($request->all());
        $rangkuman = \App\Rangkuman::find($id);
        $rangkuman->update($request->all());
        if($rangkuman->akademik()->where('akademik_id' ,$request->akademik)->exists()){
            return redirect('/rangkuman')->with('sukses', 'Data siswa sudah ada');
        }
        $rangkuman ->akademik()->attach($request->akademik);
        if($request->hasFile('avatar')){
            $request->file('avatar')->move('images/',$request->file('avatar')->getClientOriginalName());
            $rangkuman->avatar = $request->file('avatar')->getClientOriginalName();
            $rangkuman->save();
        }
        return redirect('/rangkuman')->with('sukses', 'Data berhasil diupdate');
    }

    public function delete($id)
    {
        $rangkuman = \App\Rangkuman::find($id);
        $rangkuman->delete($rangkuman);
        return redirect('/rangkuman')->with('sukses','Data berhasil dihapus');
    }

    public function penilaian($id)
    {
        $rangkuman = \App\Rangkuman::find($id);
        $siswa = \App\Siswa::select('id','user_id','no_idk','nama_lengkap','jenis_kelamin','tempat_lahir','tanggal_lahir','alamat','kelas','created_at','updated_at')->where('kelas', 'TK A')->get();
        
        $indikatorisi = \App\Indikator::all();
        $data_akademik = \App\Akademik::select('tahun_akademik')->where('status', 1)->get();
        //dd($mapel);
        //dd($indikator);
        return view('rangkuman.penilaian',['siswa' => $siswa, 'rangkuman' => $rangkuman, 'indikatorisi' => $indikatorisi,'data_akademik' => $data_akademik]);
    }

    public function penilaian2($id)
    {
        $rangkuman = \App\Rangkuman::find($id);
        $siswa = \App\Siswa::select('id','user_id','no_idk','nama_lengkap','jenis_kelamin','tempat_lahir','tanggal_lahir','alamat','kelas','created_at','updated_at')->where('kelas', 'TK A')->get();
        
        $indikatorisi = \App\Indikator::all();
        $data_akademik = \App\Akademik::select('tahun_akademik')->where('status', 1)->get();
        //dd($mapel);
        //dd($indikator);
        return view('rangkuman.penilaian2',['siswa' => $siswa, 'rangkuman' => $rangkuman, 'indikatorisi' => $indikatorisi,'data_akademik' => $data_akademik]);
    }

    public function addnilai(Request $request,$idrangkuman)
    {

        $siswa = \App\Siswa::select('id','user_id','no_idk','nama_lengkap','jenis_kelamin','tempat_lahir','tanggal_lahir','alamat','kelas','created_at','updated_at')->where('kelas', 'TK A')->get();
        $rangkuman = \App\Rangkuman::find($idrangkuman);
        if($rangkuman->siswa()->where('siswa_id' ,$request->siswa)->exists()){
            return redirect('rangkuman/' .$idrangkuman. '/penilaian')->with('error', 'Data siswa sudah ada');
        }

        

        $rangkuman->siswa()->attach($request->siswa, ['nilai' => $request->nilai]);

        // $rangkuman->siswa()->saveMany([
        //     new \App\Siswa(['nilai' => $idrangkuman])    
        // ]);

        return redirect('rangkuman/' .$idrangkuman. '/penilaian')->with('sukses', 'Data nilai berhasil ditambahkan');
    }

    public function deletenilai($idrangkuman,$idsiswa){
        $rangkuman = \App\Rangkuman::find($idrangkuman);
        $rangkuman->siswa()->detach($idsiswa);
        return redirect()->back()->with('sukses','Data berhasil dihapus');
    }
}
