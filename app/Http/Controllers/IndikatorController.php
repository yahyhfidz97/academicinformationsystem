<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndikatorController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('cari')){
            $data_indikator = \App\Indikator::where('nama_indikator','LIKE','%'.$request->cari.'%')->get();
        }else{
            $data_indikator = \App\Indikator::all();
            $data_akademik = \App\Akademik::select('tahun_akademik')->where('status', 1)->get();
        }
        
        return view('indikator.index',['data_indikator' => $data_indikator, 'data_akademik' => $data_akademik]);
    }

    public function create(Request $request)
    {
        // $this->validate($request,[ 'nama_depan' => 'min:5',
        // 'nama_belakang' => 'required',
        //     'email' => 'required|email|unique:users',
        //     'jenis_kelamin' => 'required',
        //     'agama' => 'required',
        //     'avatar' => 'mimes:jpeg,png',
        //     ]);


        //insert ke tabel users
        // $user = new \App\User;
        // $user->role = 'guru';
        // $user->name = $request->no_id_pegawai;
        // $user->email = $request->email;
        // $user->password = bcrypt('rahasia');
        // $user->remember_token = Str::random(60);
        // $user->save();

        //insert ke tabel guru
        //$request->request->add(['user_id' => $user->id]);
        $indikator = \App\Indikator::create($request->all());
        if($request->hasFile('avatar')){
            $request->file('avatar')->move('images/',$request->file('avatar')->getClientOriginalName());
            $indikator->avatar = $request->file('avatar')->getClientOriginalName();
            $indikator->save();
        }
        return redirect('/indikator')->with('sukses','Data berhasil ditambahkan');
    }

    public function edit($id)
    {
        $indikator = \App\Indikator::find($id);
        $data_akademik = \App\Akademik::all();
        return view('indikator/edit',['indikator' => $indikator, 'data_akademik' => $data_akademik]);
    }

    public function update(Request $request,$id)
    {
        //dd($request->all());
        $indikator = \App\Indikator::find($id);
        $indikator->update($request->all());
        if($request->hasFile('avatar')){
            $request->file('avatar')->move('images/',$request->file('avatar')->getClientOriginalName());
            $indikator->avatar = $request->file('avatar')->getClientOriginalName();
            $indikator->save();
        }
        return redirect('/indikator')->with('sukses', 'Data berhasil diupdate');
    }

    public function delete($id)
    {
        $indikator = \App\Indikator::find($id);
        $indikator->delete($indikator);
        return redirect('/indikator')->with('sukses','Data berhasil dihapus');
    }

}
