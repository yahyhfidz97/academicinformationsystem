@extends('layouts.master')
@section('header')

    
    <link href="{{asset('toggle/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css')}}" rel="stylesheet">
    

   

@stop
@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                            <div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Data Tahun Akademik <br>@foreach ($data_akademik as $item)
									T.A. {{$item->tahun_akademik}}
									@endforeach</h3>
                                    <br>
                                    
                                    <div class="right">
                                    <button type="button" class="btn" data-toggle="modal" data-target="#exampleModal"><i class="lnr lnr-plus-circle"></i>
                                    </button>
                                    <button type="button" class="btn no-print" onclick="javascript:printDiv('print-area-2');"><i class="lnr lnr-printer"></i></button>
                                    </div>
								</div>
								<div class="panel-body print-area" id="print-area-2">
									<table class="table table-hover" border="1" >
										<thead>
											<tr>
                                                <th>TAHUN AKADEMIK</th>
                                                <th>STATUS</th>
											</tr>
										</thead>
										<tbody>
                                        @foreach($data_akademik2 as $akademik)
                                        <tr>
                                            <td>{{$akademik->tahun_akademik}}</td>
                                            <td>
                                                <input data-id="{{$akademik->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $akademik->status ? 'checked' : '' }}>
                                            </td>
                                            <td><a href="/akademik/{{$akademik->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                                <a href="/akademik/{{$akademik->id}}/delete" class="btn btn-danger btn-sm" onClick="return confirm('Yakin mau dihapus?')">Delete</a></td>
                                        </tr>
                                        @endforeach
										</tbody>
									</table>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Indikator-->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="/akademik/create" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}

                        <div class="mb-3 {{$errors->has('tahun_akademik') ? ' has-error' : ''}}">
                            <label for="exampleInputEmail1" class="form-label">Tahun Akademik</label>
                            <input name="tahun_akademik" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tahun Akademik" 
                            value="{{old('tahun_akademik')}}">
                            @if($errors->has('tahun_akademik'))
                            <span class="help-block">{{$errors->first('tahun_akademik')}}</span>
                            @endif
                        </div>

                        


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
@endsection    

@section('toggle')
<script>
  $(function() {
    $('.toggle-class').change(function() {
        var status = $(this).prop('checked') == true ? 1 : 0; 
        var user_id = $(this).data('id'); 
        let _token   = $('meta[name="csrf-token"]').attr('content');
        

        $.ajax({
            type: "GET",
            dataType: "json",
            url: '/changeStatus',
            data: {'status': status, 'user_id': user_id, '_token': _token},
            success: function(data){
              console.log(data.success)
              location.reload();
            }
        });

    })
  })
</script>




@stop
