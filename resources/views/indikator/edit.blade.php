@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
							<div class="panel-heading">
							<h3 class="panel-title">Inputs</h3>
							</div>
							<div class="panel-body">
                            <form action="/indikator/{{$indikator->id}}/update" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}

                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Kode</label>
                            <input name="kode" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Kode" value="{{$indikator->kode}}">
                        </div>

                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Nama Indikator</label>
                            <input name="nama_indikator" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Indikator" value="{{$indikator->nama_indikator}}">
                        </div>

        

                        
                        
                        <button type="submit" class="btn btn-warning">Update</button>
                    </form>

						</div>
				    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop