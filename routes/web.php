<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auths/login');
});

Route::get('/login','AuthController@Login')->name('login');
Route::post('/postlogin','AuthController@postlogin');
Route::get('/logout','AuthController@logout');


Route::group(['middleware'=>['auth','checkRole:admin,guru,guru2']],function(){
    Route::get('/dashboards','DashboardController@index');
    Route::post('/dashboards/create','DashboardController@create');
    Route::get('/dashboards/{id}/edit','DashboardController@edit');
    Route::post('/dashboards/{id}/update','DashboardController@update');
    Route::post('/dashboards/{id}/delete','DashboardController@delete');
    

    Route::get('/siswa','SiswaController@index');
    Route::get('/siswa2','SiswaController@index2');
    
    Route::get('/siswa/kelas','SiswaController@kelas');
    Route::get('/siswa/kelas2','SiswaController@kelas2');

    Route::post('/siswa/create','SiswaController@create');
    Route::get('/siswa/{id}/edit','SiswaController@edit');
    Route::post('/siswa/{id}/update','SiswaController@update');
    Route::get('/siswa/{id}/delete','SiswaController@delete');
    Route::get('/siswa/{id}/profile','SiswaController@profile');
    Route::post('/siswa/{id}/addnilai','SiswaController@addnilai');
    Route::get('/siswa/{id}/{idmapel}/deletenilai','SiswaController@deletenilai');

    Route::post('/siswa/{id}/addindikator','SiswaController@addindikator');
    Route::get('/siswa/{id}/{idindikator}/deleteindikator','SiswaController@deleteindikator');

    Route::get('/rangkuman','RangkumanController@index');
    Route::get('/rangkuman2','RangkumanController@index2');
    Route::post('/rangkuman/create','RangkumanController@create');
    Route::get('/rangkuman/{id}/edit','RangkumanController@edit');
    Route::post('/rangkuman/{id}/update','RangkumanController@update');
    Route::get('/rangkuman/{id}/delete','RangkumanController@delete');
    Route::get('/rangkuman/{id}/penilaian','RangkumanController@penilaian');
    Route::get('/rangkuman/{id}/penilaian2','RangkumanController@penilaian2');
    Route::post('/rangkuman/{id}/addnilai','RangkumanController@addnilai');
    Route::get('/rangkuman/{id}/{idrangkuman}/deletenilai','RangkumanController@deletenilai');

    Route::get('/indikator','IndikatorController@index');
    Route::post('/indikator/create','IndikatorController@create');
    Route::get('/indikator/{id}/edit','IndikatorController@edit');
    Route::post('/indikator/{id}/update','IndikatorController@update');
    Route::get('/indikator/{id}/delete','IndikatorController@delete');

    Route::get('/kelas','KelasController@index');
    Route::post('/kelas/create','KelasController@create');
    Route::get('/kelas/{id}/edit','KelasController@edit');
    Route::post('/kelas/{id}/update','KelasController@update');
    Route::get('/kelas/{id}/delete','KelasController@delete');
    Route::get('/kelas_siswa','KelasController@kelas_siswa');


Route::get('/guru','GuruController@index');
Route::post('/guru/create','GuruController@create');
Route::get('/guru/{id}/edit','GuruController@edit');
Route::post('/guru/{id}/update','GuruController@update');
Route::get('/guru/{id}/delete','GuruController@delete');

Route::get('/akademik','AkademikController@index');
Route::get('changeStatus', 'AkademikController@changeStatus');
Route::post('/akademik/create','AkademikController@create');
Route::get('/akademik/{id}/edit','AkademikController@edit');
Route::post('/akademik/{id}/update','AkademikController@update');
Route::get('/akademik/{id}/delete','AkademikController@delete');

});




Route::group(['middleware'=>['auth','checkRole:admin,siswa,guru,guru2']],function(){
    Route::get('/dashboards','DashboardController@index');
    Route::get('/siswa','SiswaController@index');
    Route::get('/siswa2','SiswaController@index2');
    Route::get('/siswa/{id}/profile2','SiswaController@profile2');
});