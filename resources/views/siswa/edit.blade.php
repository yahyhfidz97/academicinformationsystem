@extends('layouts.master')

@section('content')
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Inputs</h3>
								</div>
								<div class="panel-body">
                                <form action="/siswa/{{$siswa->id}}/update" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Nomor Induk</label>
                            <input name="no_idk" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nomor Induk" value="{{$siswa->no_idk}}">
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Nama Lengkap</label>
                            <input name="nama_lengkap" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Lengkap" value="{{$siswa->nama_lengkap}}">
                        </div>
                        
                        <label for="jenis_kelamin" class="form-label">Jenis Kelamin</label>
                                <select id="jenis_kelamin" class="form-control" name="jenis_kelamin">
                                    <option selected value="L" @if ($siswa -> jenis_kelamin == 'L') selected @endif
                                        >Laki-Laki</option>
                                    <option value="P" @if ($siswa -> jenis_kelamin == 'P') selected @endif >Perempuan
                                    </option>
                                </select>

                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Tempat Lahir</label>
                            <input name="tempat_lahir" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tempat Lahir" value="{{$siswa->tempat_lahir}}">
                        </div>

                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Tanggal lahir</label>
                            <input name="tanggal_lahir" type="date" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tanggal Lahir" value="{{$siswa->tanggal_lahir}}">
                        </div>

                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Alamat</label>
                            <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3">{{$siswa->alamat}}</textarea>
                        </div>

                                <label for="kelas" class="form-label">Kelas</label>
                                <select id="kelas" class="form-control" name="kelas">
                                    <option selected value="TK A" @if ($siswa -> kelas == 'TK A') selected @endif >TK A</option>
                                    <option value="TK B" @if ($siswa -> kelas == 'TK B') selected @endif >TK B</option>
                                </select>

                                <div class="mb-3">
						<label for="akademik">Tahun Akademik</label>
						<select class="form-control" id="akademik" name="akademik">
						@foreach($akademikisi as $akd)
						<option value="{{$akd->id}}">{{$akd->tahun_akademik}}</option>
						@endforeach
						</select>
						</div>
                        
                        <button type="submit" class="btn btn-warning">Update</button>
                    </form>
								</div>
							</div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
